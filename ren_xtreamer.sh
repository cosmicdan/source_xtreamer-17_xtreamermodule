if [ ! -d ./src/com/ceco/gm2 ]; then
    echo "Already renamed or not in script folder"
    exit
fi

rm -rf ./gen
sed -i 's/com\.ceco\.gm2/net\.xtreamer\.tweaks/g' ./AndroidManifest.xml
find ./src/ -type f -exec sed -i 's/com\.ceco\.gm2/net\.xtreamer\.tweaks/g' {} +
find ./res/ -type f -iname "*.xml" -exec sed -i 's/com\.ceco\.gm2/net\.xtreamer\.tweaks/g' {} +
mkdir ./src/net/xtreamer
mv ./src/com/ceco/gm2 ./src/net/xtreamer/tweaks
rm -rf ./src/com