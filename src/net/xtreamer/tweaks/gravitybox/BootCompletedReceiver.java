/*
 * Copyright (C) 2014 Peter Gregus for GravityBox Project (C3C076@xda)
 * Additional changes by Daniel Connolly under Xtreamer Mobile, 2014.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.xtreamer.tweaks.gravitybox;

import java.io.File;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class BootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            prepareAssets(context);
        }
    }

    // copies required files from assets to file system
    private void prepareAssets(Context context) {
        File f;

        // prepare battery sound files
        final String[] bSounds = new String[] { "battery_charged.ogg", "charger_plugged.ogg", "charger_unplugged.ogg" };
        for (String bSound : bSounds) {
            f = new File(context.getFilesDir() + "/" + bSound);
            if (!f.exists()) {
                Utils.writeAssetToFile(context, bSound, f);
            }
            if (f.exists()) {
                f.setReadable(true, false);
            }
        }

        // [XT] set default lockscreen targets if required
        SharedPreferences mPrefs = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_WORLD_READABLE);
        if (mPrefs.getBoolean("pref_freshdata", true)) {
            Editor e = mPrefs.edit();
            e.putString("pref_lockscreen_ring_targets_app0", "#Intent;action=androidshortcuts.intent.action.LAUNCH_ACTION;launchFlags=0x10008000;component=net.xtreamer.androidshortcuts/.ShortcutActivity;S.action=Phone;S.icon=%2Fsystem%2Fresources%2Fshortcut_phone%2Epng;S.prefLabel=Common%20Shortcuts%3A%20Phone;S.label=Phone;S.actionType=broadcast;i.mode=1;end");
            e.putString("pref_lockscreen_ring_targets_app1", "#Intent;action=androidshortcuts.intent.action.LAUNCH_ACTION;launchFlags=0x10008000;component=net.xtreamer.androidshortcuts/.ShortcutActivity;S.action=GoogleNow;S.icon=%2Fsystem%2Fresources%2Fshortcut_google_now%2Epng;S.prefLabel=Common%20Shortcuts%3A%20Google%20Now;S.label=Google%20Now;S.actionType=broadcast;i.mode=1;end");
            e.putString("pref_lockscreen_ring_targets_app2", "#Intent;action=androidshortcuts.intent.action.LAUNCH_ACTION;launchFlags=0x10008000;component=net.xtreamer.androidshortcuts/.ShortcutActivity;S.action=Messaging;S.icon=%2Fsystem%2Fresources%2Fshortcut_messaging%2Epng;S.prefLabel=Common%20Shortcuts%3A%20Messaging;S.label=Messaging;S.actionType=broadcast;i.mode=1;end");
            e.putString("pref_lockscreen_ring_targets_app3", "#Intent;action=androidshortcuts.intent.action.LAUNCH_ACTION;launchFlags=0x10008000;component=net.xtreamer.androidshortcuts/.ShortcutActivity;S.action=Camera;S.icon=%2Fsystem%2Fresources%2Fshortcut_camera%2Epng;S.prefLabel=Common%20Shortcuts%3A%20Camera;S.label=Camera;S.actionType=broadcast;i.mode=1;end");
            e.putString("pref_lockscreen_ring_targets_app4", "#Intent;action=androidshortcuts.intent.action.LAUNCH_ACTION;launchFlags=0x10008000;component=net.xtreamer.androidshortcuts/.ShortcutActivity;S.action=Contacts;S.icon=%2Fsystem%2Fresources%2Fshortcut_contacts%2Epng;S.prefLabel=Common%20Shortcuts%3A%20Contacts;S.label=Contacts;S.actionType=broadcast;i.mode=1;end");
            e.putBoolean("pref_lockscreen_ring_targets_enable", true);
            e.putBoolean("pref_freshdata", false);
            e.commit();
        }
        // [/XT]
    }
}
