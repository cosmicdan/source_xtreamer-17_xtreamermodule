/*
 * Copyright (C) 2013 Peter Gregus for GravityBox Project (C3C076@xda)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.xtreamer.tweaks.gravitybox;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import static de.robv.android.xposed.XposedHelpers.findClass;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceActivity.Header;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.callbacks.XC_InitPackageResources.InitPackageResourcesParam;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class GravityBox implements IXposedHookZygoteInit, IXposedHookInitPackageResources, IXposedHookLoadPackage {
    public static final String PACKAGE_NAME = GravityBox.class.getPackage().getName();
    public static String MODULE_PATH = null;
    private static XSharedPreferences prefs;

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        MODULE_PATH = startupParam.modulePath;
        prefs = new XSharedPreferences(PACKAGE_NAME);
        prefs.makeWorldReadable();

        XposedBridge.log("[XT] SoC: " + Build.HARDWARE);
        XposedBridge.log("[XT] Product: " + Build.PRODUCT);
        //XposedBridge.log("[XT] Device manufacturer: " + Build.MANUFACTURER);
        //XposedBridge.log("[XT] GravityBox: Device brand: " + Build.BRAND);
        XposedBridge.log("[XT] Device model: " + Build.MODEL);
        XposedBridge.log("[XT] Device type: " + (Utils.isTablet() ? "tablet" : "phone"));
        XposedBridge.log("[XT] Is MTK device: " + Utils.isMtkDevice());
        //XposedBridge.log("[XT] Is Xperia device: " + Utils.isXperiaDevice());
        //XposedBridge.log("[XT] Has Lenovo custom UI: " + Utils.hasLenovoCustomUI());
        if (Utils.hasLenovoCustomUI()) {
            //XposedBridge.log("[XT] Lenovo UI is VIBE: " + Utils.hasLenovoVibeUI());
            //XposedBridge.log("[XT] Lenovo ROM is ROW: " + Utils.isLenovoROW());
        }
        XposedBridge.log("[XT] Has telephony support: " + Utils.hasTelephonySupport());
        XposedBridge.log("[XT] Has Gemini support: " + Utils.hasGeminiSupport());
        XposedBridge.log("[XT] Android Platform: " + Build.VERSION.SDK_INT);
        XposedBridge.log("[XT] Android Release: " + Build.VERSION.RELEASE);
        XposedBridge.log("[XT] Build ID: " + Build.DISPLAY);

        SystemWideResources.initResources(prefs);

        // MTK specific
        if (Utils.isMtkDevice()) {
            if (Utils.hasGeminiSupport()) {
                ModSignalIconHide.initZygote(prefs);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_CALLER_ID_PHONE, false)) {
                FixCallerIdPhone.initZygote(prefs);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_DEV_OPTS, false)) {
                FixDevOptions.initZygote();
            }

            if ((Build.VERSION.SDK_INT == Build.VERSION_CODES.JELLY_BEAN_MR1) &&
                    prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_LOCATION, false)) {
                FixLocation.initZygote();
            }
        }

        // 4.2+ only
        if (Build.VERSION.SDK_INT > 16) {
            FixTraceFlood.initZygote();
            ModElectronBeam.initZygote(prefs);
            if (!Utils.hasLenovoVibeUI()) {
                ModLockscreen.initZygote(prefs);

                // PermissionGranter init goes here because so far its only purpose
                // is to grant permissions needed by new QuickSettings custom tiles
                PermissionGranter.initZygote();
            }
        }

        // Common
        ModVolumeKeySkipTrack.init(prefs);
        ModInputMethod.initZygote(prefs);
        ModCallCard.initZygote();
        ModStatusbarColor.initZygote(prefs);
        PhoneWrapper.initZygote(prefs);
        ModLowBatteryWarning.initZygote(prefs);
        ModDisplay.initZygote(prefs);
        ModAudio.initZygote(prefs);
        ModHwKeys.initZygote(prefs);
        PatchMasterKey.initZygote();
        ModPhone.initZygote(prefs);
        ModExpandedDesktop.initZygote(prefs);
        ConnectivityServiceWrapper.initZygote();
        ModViewConfig.initZygote(prefs);

        if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_NAVBAR_OVERRIDE, false)) {
            ModNavigationBar.initZygote(prefs);
        }

        ModLedControl.initZygote();
    }

    @Override
    public void handleInitPackageResources(InitPackageResourcesParam resparam) throws Throwable {

        if (resparam.packageName.equals(ModBatteryStyle.PACKAGE_NAME))
            ModBatteryStyle.initResources(prefs, resparam);

        if (resparam.packageName.equals(ModStatusBar.PACKAGE_NAME)) {
            ModStatusBar.initResources(prefs, resparam);
        }

        if (resparam.packageName.equals(FixDevOptions.PACKAGE_NAME)) {
            FixDevOptions.initPackageResources(prefs, resparam);
        }

        if (Build.VERSION.SDK_INT > 16 && !Utils.hasLenovoVibeUI() &&
                resparam.packageName.equals(ModQuickSettings.PACKAGE_NAME)) {
            ModQuickSettings.initResources(prefs, resparam);
        }
    }

    @Override
    public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {

        if (lpparam.packageName.equals(SystemPropertyProvider.PACKAGE_NAME)) {
            SystemPropertyProvider.init(lpparam.classLoader);
        }

        // MTK Specific
        if (Utils.isMtkDevice()) {
            if (Utils.hasGeminiSupport() &&
                    lpparam.packageName.equals(ModSignalIconHide.PACKAGE_NAME)) {
                ModSignalIconHide.init(prefs, lpparam.classLoader);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_CALLER_ID_MMS, false) &&
                    lpparam.packageName.equals(FixCallerIdMms.PACKAGE_NAME)) {
                FixCallerIdMms.init(prefs, lpparam.classLoader);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_CALENDAR, false) &&
                    lpparam.packageName.equals(FixCalendar.PACKAGE_NAME)) {
                FixCalendar.init(prefs, lpparam.classLoader);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_DATETIME_CRASH, false) &&
                    lpparam.packageName.equals(FixDateTimeCrash.PACKAGE_NAME)) {
                FixDateTimeCrash.init(prefs, lpparam.classLoader);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_TTS_SETTINGS, false) &&
                    lpparam.packageName.equals(FixTtsSettings.PACKAGE_NAME)) {
                FixTtsSettings.init(prefs, lpparam.classLoader);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_DEV_OPTS, false) &&
                    lpparam.packageName.equals(FixDevOptions.PACKAGE_NAME)) {
                FixDevOptions.init(prefs, lpparam.classLoader);
            }

            if (prefs.getBoolean(GravityBoxSettings.PREF_KEY_FIX_MMS_WAKELOCK, false) && 
                    lpparam.packageName.equals(FixMmsWakelock.PACKAGE_NAME)) {
                FixMmsWakelock.init(prefs, lpparam.classLoader);
            }

            if (lpparam.packageName.equals(ModAudioSettings.PACKAGE_NAME)) {
                ModAudioSettings.init(prefs, lpparam.classLoader);
            }

            if (lpparam.packageName.equals(ModCellConnService.PACKAGE_NAME)) {
                ModCellConnService.init(prefs, lpparam.classLoader);
            }

            if (Build.VERSION.SDK_INT > 16
                    && Utils.hasGeminiSupport()
                    && !Utils.hasLenovoCustomUI()
                    && lpparam.packageName.equals(ModMtkToolbar.PACKAGE_NAME)) {
                ModMtkToolbar.init(prefs, lpparam.classLoader);
            }

            if (Utils.hasGeminiSupport()
                    && lpparam.packageName.equals(ModStatusBar.PACKAGE_NAME)) {
                ModStatusBar.initMtkPlugin(prefs, lpparam.classLoader);
            }
        }

        // Common
        if (lpparam.packageName.equals(ModBatteryStyle.PACKAGE_NAME)) {
            ModBatteryStyle.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModLowBatteryWarning.PACKAGE_NAME)) {
            ModLowBatteryWarning.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModClearAllRecents.PACKAGE_NAME)) {
            ModClearAllRecents.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModPowerMenu.PACKAGE_NAME)) {
            ModPowerMenu.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModCallCard.PACKAGE_NAME)) {
            ModCallCard.init(prefs, lpparam.classLoader);
        }

        if (Build.VERSION.SDK_INT > 16 && !Utils.hasLenovoVibeUI() &&
                lpparam.packageName.equals(ModQuickSettings.PACKAGE_NAME) &&
                prefs.getBoolean(GravityBoxSettings.PREF_KEY_QUICK_SETTINGS_ENABLE, true)) {
            ModQuickSettings.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModStatusbarColor.PACKAGE_NAME)) {
            ModStatusbarColor.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModStatusBar.PACKAGE_NAME)) {
            ModStatusBar.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModPhone.PACKAGE_NAME) &&
                Utils.hasTelephonySupport()) {
            ModPhone.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModSettings.PACKAGE_NAME)) {
            ModSettings.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModVolumePanel.PACKAGE_NAME)) {
            ModVolumePanel.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModPieControls.PACKAGE_NAME)) {
            ModPieControls.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModNavigationBar.PACKAGE_NAME)
                && prefs.getBoolean(GravityBoxSettings.PREF_KEY_NAVBAR_OVERRIDE, false)) {
            ModNavigationBar.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModMms.PACKAGE_NAME)) {
            ModMms.init(prefs, lpparam.classLoader);
        }

        if (ModLauncher.PACKAGE_NAMES.equals(lpparam.packageName)) {
            ModLauncher.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModSmartRadio.PACKAGE_NAME) &&
                prefs.getBoolean(GravityBoxSettings.PREF_KEY_SMART_RADIO_ENABLE, false)) {
            ModSmartRadio.init(prefs, lpparam.classLoader);
        }

        if (Build.VERSION.SDK_INT > 17 &&
                lpparam.packageName.equals(ModDownloadProvider.PACKAGE_NAME)) {
            ModDownloadProvider.init(prefs, lpparam.classLoader);
        }

        if (lpparam.packageName.equals(ModRinger.PACKAGE_NAME)) {
            ModRinger.init(prefs, lpparam.classLoader);
        }
        
        // [XT] PreferenceInjector
        if (!lpparam.packageName.equals("com.android.settings"))
            return;

        Class<?> SettingsClazz = findClass("com.android.settings.Settings", lpparam.classLoader);
        final Class<?> HeaderAdapter = findClass("com.android.settings.Settings$HeaderAdapter", lpparam.classLoader);

        findAndHookMethod(SettingsClazz, "onCreate", Bundle.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                PreferenceActivity activity = (PreferenceActivity) param.thisObject;

                List<Header> appsHeader = new ArrayList<Header>();
                XposedHelpers.setAdditionalInstanceField(activity, KEY_HEADER_LIST, appsHeader);

                ModuleLoader loader = new ModuleLoader(activity, appsHeader);
                XposedHelpers.setAdditionalInstanceField(activity, KEY_LOADER, loader);
            }
        });

        XposedHelpers.findAndHookMethod(SettingsClazz, "onResume", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                PreferenceActivity activity = (PreferenceActivity) param.thisObject;
                ModuleLoader loader = (ModuleLoader) XposedHelpers.getAdditionalInstanceField(activity, KEY_LOADER);
                loader.updateModuleList();
            }
        });

        XposedHelpers.findAndHookMethod(SettingsClazz, "onPause", new XC_MethodHook() {

            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                PreferenceActivity activity = (PreferenceActivity) param.thisObject;
                ModuleLoader loader = (ModuleLoader) XposedHelpers.getAdditionalInstanceField(activity, KEY_LOADER);
                loader.stopTask();
            }
        });

        XposedBridge.hookAllMethods(SettingsClazz, "updateHeaderList", new XC_MethodHook() {
            @SuppressWarnings("unchecked")
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                PreferenceActivity activity = (PreferenceActivity) param.thisObject;

                try {
                    ActionBar actionBar = activity.getActionBar();
                    if (actionBar.getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS) {
                        // A Samsung
                        if (actionBar.getSelectedNavigationIndex() != actionBar.getNavigationItemCount() - 1) {
                            return;
                        }
                    }
                } catch (Throwable t) {

                }

                List<Header> headers = (List<Header>) param.args[0];
                List<Header> appsHeader = (List<Header>) XposedHelpers.getAdditionalInstanceField(activity,
                        KEY_HEADER_LIST);
                if (appsHeader == null)
                    appsHeader = new ArrayList<Header>();

                // Find headerIndex
                int headerIndex = findHeaderIndex(activity, headers, "system_section");
                if (headerIndex == -1) {
                    headerIndex = headers.size();
                } else {
                    // Inject BEFORE this section.
                    headerIndex--;
                }

                // Add section
                if (sSectionHeader == null) {
                    sSectionHeader = new Header();
                    sSectionHeader.title = "Xtreamer";
                }
                headers.add(headerIndex++, sSectionHeader);

                // Add apps header
                headers.addAll(headerIndex++, appsHeader);
                headerIndex += appsHeader.size() - 1;

                param.args[0] = headers;
            }
        });

        /*
         * We can only pass resIds above, so we override the getView method to
         * load the icon manually
         */
        findAndHookMethod(HeaderAdapter, "getView", int.class, View.class, ViewGroup.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                BaseAdapter adapter = (BaseAdapter) param.thisObject;
                Header header = (Header) adapter.getItem((Integer) param.args[0]);
                if (header.extras != null) {
                    boolean isXposedModule = header.extras.getBoolean("xposed_module", false);
                    if (isXposedModule) {
                        String packageName = header.extras.getString("xposed_package_name");
                        View view = (View) param.getResult();
                        ImageView icon = (ImageView) XposedHelpers.getObjectField(view.getTag(), "icon");
                        icon.setAdjustViewBounds(true);
                        // TODO Move to async
                        icon.setImageDrawable(view.getContext().getPackageManager().getApplicationIcon(packageName));
                    }
                }
            }
        });
    }
    
    // [XT] Preference Injector
    @SuppressWarnings("unused")
    private static final String TAG = "InjectXposedPreference";
    private static final String KEY_LOADER = "ModuleLoader";
    private static final String KEY_HEADER_LIST = "HeaderList";
    private static Header sSectionHeader;
    private static Header sMoreHeader;
    private static WeakReference<Resources> sRefResources;

    static Header getHeaderFromAppInfo(PackageManager pm, ApplicationInfo info) {
        return getHeaderFromAppInfo(pm, info, null);
    }

    public static Header getHeaderFromAppInfo(PackageManager pm, ApplicationInfo info, String title) {
        Header header = new Header();
        header.title = (title == null) ? info.loadLabel(pm) : title;
        header.iconRes = android.R.drawable.sym_def_app_icon;

        Bundle extras = new Bundle();
        extras.putString("xposed_package_name", info.packageName);
        extras.putBoolean("xposed_module", true);
        header.extras = extras;

        return header;
    }

    public static String getStringSettingsPackage(Activity activity, String name, String def_text) {
        Resources res = activity.getResources();
        try {
            int resId = res.getIdentifier(name, "string", activity.getPackageName());
            if (resId == 0)
                return def_text;
            return res.getString(resId);
        } catch (Exception e) {
            return def_text;
        }
    }

    public static String getStringMyPackage(Context context, int resId, String def_text) {
        Resources res;
        if (sRefResources == null || sRefResources.get() == null) {
            PackageManager pm = context.getPackageManager();
            try {
                res = pm.getResourcesForApplication(GravityBox.class.getPackage().getName());
            } catch (NameNotFoundException e) {
                return def_text;
            }
            sRefResources = new WeakReference<Resources>(res);
        } else {
            res = sRefResources.get();
        }
        try {
            return res.getString(resId);
        } catch (Exception e) {
            return def_text;
        }
    }

    public static int findHeaderIndex(Activity activity, List<Header> headers, String headerName) {
        int headerIndex = -1;
        int resId = activity.getResources().getIdentifier(headerName, "id", activity.getPackageName());
        if (resId != 0) {
            int i = 0;
            int size = headers.size();
            while (i < size) {
                Header header = headers.get(i);
                int id = (int) header.id;
                if (id == resId) {
                    headerIndex = i + 1;
                    break;
                }
                i++;
            }
        }
        return headerIndex;

    }
    
    
}