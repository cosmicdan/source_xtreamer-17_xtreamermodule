GravityBox
==========

Xposed Tweak Box for devices running Android 4.1/4.2/4.3 (JellyBean)

[Xtreamer] Additional modifications and expansions (c) 2014 Daniel Connolly under Xtreamer Mobile.
All significant Xtreamer Mobile additions/changes are tagged with [XT] comments.
http://www.xtreamermobile.com

Visit official thread at XDA for more info on GravityBox (but do not report bugs for this modified version):
http://forum.xda-developers.com/showthread.php?t=2316070

Copyright (C) 2013 Peter Gregus (xgravitybox@gmail.com)
You may not distribute nor sell this software or parts of it in 
Source, Object nor in any other form without explicit permission obtained 
from the original author.

[Xtreamer] As per Apache 2.0 sections 2 (Grant of Copyright) and 4 (Redistribution), 
the above explicit permission is automatically granted (while additional terms of 
copyright and redistribution are permitted, the Apache 2.0 terms can not be picked
apart in such a way). However, Xtreamer Mobile decided to retain the original licence 
without re-licensing and kept this software open-source - despite not being required 
to - with the best intentions to retain share-alike development among the community.

[Xtreamer] While Apache 2.0 license lists an "as is" basis of warranty and revokes
any implied liability, Xtreamer Mobile products that utilize "Xtreamer Gravitybox" are
accompanied with general guarantees and warranty of international merchantability, as
appropriate for Xtreamer Mobile products (this is generally the case with any Copyleft
license). This guarantee does NOT apply to any use of this source-code when compiled
and used outside of Xtreamer Mobile production and deployment, even if used on Xtreamer
Mobile products (in simple terms; unofficial use is not supported in any way).
