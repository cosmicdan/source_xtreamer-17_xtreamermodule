if [ ! -d ./src/net/xtreamer/tweaks ]; then
    echo "Already renamed or not in script folder"
    exit
fi

rm -rf ./gen
sed -i 's/net\.xtreamer\.tweaks/com\.ceco\.gm2/g' ./AndroidManifest.xml
find ./src/ -type f -exec sed -i 's/net\.xtreamer\.tweaks/com\.ceco\.gm2/g' {} +
find ./res/ -type f -iname "*.xml" -exec sed -i 's/net\.xtreamer\.tweaks/com\.ceco\.gm2/g' {} +
mkdir ./src/com
mkdir ./src/com/ceco
mv ./src/net/xtreamer/tweaks ./src/com/ceco/gm2
rm -rf ./src/net/xtreamer